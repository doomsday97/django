from django.contrib import admin

from .models import Choice, Question
from material import *

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    layout= Layout( Row('question_text', 'pub_date', 'was_published_recently'))
    inlines = [ChoiceInline]
    list_filter = ['pub_date']


admin.site.register(Question, QuestionAdmin)